Proyecto para probar conceptos de:

- AOP / Metrics
- Logback DBAppender
- JMS

Para aplicar a IAP.

Se puede generar como (Spring Boot) jar o como war.

Cuando se genera a WAR se incluyen los archivos application.properties y logback.xml desde src/main/release/
(tuve que torcerle un poco el brazo a la tarea para que no copiara los que est�n en src/main/resource)
Por defecto la tarea war copia todo lo que est� en webapp a la ra�z del war generado, por ejemplo WEB-INF/jnoss-web.xml

Probado con JBoss 7 EAP.

NOTA:
Debe ejecutar el JBoss usando standalone-full.xml que incluye todo el stack JEE.
standalone.xml solo incluye el Web Profile (no JMS entre otros).

AOP / Metrics: com.iap.activity.interceptors.ActivityLogInterceptor
*************
Prueba de concepto de contadores.
Intercepta: "execution(* com.iap.controller.*.*(..))"
Ejecute desde un browser o Postamn http://localhost:9000/api/hello/{name} y ver� que se escribe en el logger "activityLog".
Por defecto manda a consola pero la intenci�n es que mandemos la salida a BD o a donde convenga.

Cuando corre en JBoss el contexto es /aop-demo (lo puede cambiar en src/main/release/WEB-INF/jnoss-web.xml

Logback DBAppender:
******************
Para escribir a bd hay que configurar el appender activityLogDB en logback.xml.
Se puede escribir a cualquier base de datos soportada (https://logback.qos.ch/manual/appenders.html#DBAppender)
Los scripts de creaci�n est� en la distribuci�n de logback (https://logback.qos.ch/download.html) en 
<logback-zip o dir>\logback-classic\src\main\resources\ch\qos\logback\classic\db\script\
Si quiere experimentar con un data source de JBoss:
- Configure un data source en JBoss
- Use un JNDIConnectionSource en logback.xml indicando en el elemento <jndiLocation> el nombre del data source en el JNDI.

N�tese que en logback.xml hay comentado un AsyncAppender.
Dado que la aplicaci�n puede tener muchas peticiones y que se genere mucho log de BD, para evitar que la
app se "retrase" se manda el log al appender as�ncrono, es decir, no se espera a que la operaci�n hacia la BD
se termine para continuar trabajando. Mas informaci�n del AsyncAppender aca:
https://logback.qos.ch/manual/appenders.html#AsyncAppender

JMS: com.iap.jmstest.*
***
MessageSender env�a a la cola un mensaje cada 5s.
MessageReceiver recupera los mensajes de la cola y escribe el resultado en un logger, por defecto a consola.

El war corre en JBoss, crear la cola jms/testQueue con los medios habituales (cli o consola de administraci�n).
EL nombre JNDI debe ser java:/jms/testQueue

No es necesario crear el bean JmsListenerContainerFactory / DefaultJmsListenerContainerFactory 
como est� en los ejemplos (https://spring.io/guides/gs/messaging-jms/ o en http://www.baeldung.com/spring-jms),
basta con no incluir en el @JmsListener el containerFactory. Solo en el caso de que queramos usar otra 
f�brica ser�a necesario indicarlo @JmsListener y en el JmsTemplate.

N�tese que para JBoss en application.properties la llave spring.jms.jndi-name apunta al 
JMS connection factory defecto del servidor. Si se usa otra connection factory simplemente
se pone all� el nombre en el JNDI.

Sergio Samayoa - 2018.06.14
