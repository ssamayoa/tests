package com.iap.controller;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/hello")
public class HelloController {
	
	private static Logger logger = LoggerFactory.getLogger(HelloController.class);
	
	// Need to be moved somewhere else, "Constants" class perhaps?
	// This must be added to the MDC when applies by app code.
	public static final String CORRELATOR_ID = "correlatorId"; 
	public static final String IMSI = "imsi";
	
	// This must be set by auth service.
	public static final String USER_ID = "userId";
	// Initially set by auth service but operating on other non user data must be set by app code.
	public static final String ORGANIZATION_ID = "organizationId";
	
	@RequestMapping(value = "/{name}", method = RequestMethod.GET)
	public String hello(@PathVariable String name) {
		String correltorId = UUID.randomUUID().toString();
		logger.debug("correlatorId: {}", correltorId);
		MDC.put(CORRELATOR_ID, correltorId);
		return "Hello " + name + "!";
	}

}
