package com.iap.activity.interceptors;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.MDC;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.MetricRegistry;

@Aspect
@Component
public class ActivityLogInterceptor {

	private Logger activityLog = LoggerFactory.getLogger("activityLog");

	private MetricRegistry metrics = new MetricRegistry();

	// Shared in "Constants" utility class? TBD
	public static final String SYSTEM_ID = "systemId";
	public static final String COMPONENT_ID = "componentId";
	public static final String OPERATION_ID = "operationId";
	public static final String ERROR_CODE_ID = "errorCode";

	// get from some external source such database or XML, TBD
	private Map<String, MetricsComponentInfo> components;

	public ActivityLogInterceptor() {
		components = new HashMap<>();
		// Just for demo purpose: HelloController.hello()
		MetricsComponentInfo componentInfo = new MetricsComponentInfo();
		componentInfo.setName("HelloController");
		componentInfo.setSystemId(101);
		componentInfo.setComponentId(100);
		MetricsMethodInfo methodInfo = new MetricsMethodInfo(componentInfo);
		methodInfo.setName("hello");
		methodInfo.setLogInvocation(true);
		methodInfo.setCountInvocation(true);
		methodInfo.assignDefaultCounterNames() ;
		componentInfo.getMethods().put(methodInfo.getName(), methodInfo);
		components.put(componentInfo.getName(), componentInfo);

		// Demo!
		// There is a slf4j reporter but lets send to console for now.
		ConsoleReporter reporter = ConsoleReporter.forRegistry(metrics).convertRatesTo(TimeUnit.SECONDS)
				.convertDurationsTo(TimeUnit.MILLISECONDS).build();
		reporter.start(30, TimeUnit.SECONDS);
	}

	private void processBefore(ProceedingJoinPoint p) {
		String className = p.getTarget().getClass().getSimpleName();
		MetricsComponentInfo componentInfo = components.get(className);
		if (componentInfo == null) {
			return;
		}
		String methodName = p.getSignature().getName();
		MetricsMethodInfo methodInfo = componentInfo.getMethods().get(methodName);
		if (methodInfo == null) {
			return;
		}
		if (methodInfo.isCountInvocation() && methodInfo.getInvocationCounterName() != null) {
			metrics.counter(methodInfo.getInvocationCounterName()).inc();
		}
	}

	private void processAfter(ProceedingJoinPoint p, Object result) {
		String className = p.getTarget().getClass().getSimpleName();
		MetricsComponentInfo componentInfo = components.get(className);
		if (componentInfo == null) {
			return;
		}
		String methodName = p.getSignature().getName();
		MetricsMethodInfo methodInfo = componentInfo.getMethods().get(methodName);
		if (methodInfo == null) {
			return;
		}
		if (methodInfo.logInvocation) {
			MDC.put(SYSTEM_ID, componentInfo.getSystemId());
			MDC.put(COMPONENT_ID, componentInfo.getComponentId());
			MDC.put(OPERATION_ID, methodInfo.getOperationId());
			activityLog.info("{}:{} invoked", className, methodName);
		}
		// Count success invocations
		// Count success or failed "business" invocations (ApiResult.status)
	}

	private void processException(ProceedingJoinPoint p, Throwable t) {
		// Count exceptions
	}

	@Around("execution(* com.iap.controller.*.*(..))")
	public Object webLogging(ProceedingJoinPoint p) throws Throwable {
		try {
			processBefore(p);
			Object result = p.proceed();
			processAfter(p, result);
			return result;
		} catch (Throwable t) {
			processException(p, t);
			throw t;
		}
	}

}
