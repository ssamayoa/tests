package com.iap.activity.interceptors;

import java.util.HashMap;
import java.util.Map;

public class MetricsComponentInfo {
	private String name;
	private int systemId;
	private int componentId;
	private Map<String, MetricsMethodInfo> methods = new HashMap<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSystemId() {
		return systemId;
	}

	public void setSystemId(int systemId) {
		this.systemId = systemId;
	}

	public int getComponentId() {
		return componentId;
	}

	public void setComponentId(int componentId) {
		this.componentId = componentId;
	}

	public Map<String, MetricsMethodInfo> getMethods() {
		return methods;
	}

	public void setMethods(Map<String, MetricsMethodInfo> methods) {
		this.methods = methods;
	}

}