package com.iap.activity.interceptors;

import javax.validation.constraints.NotNull;

public class MetricsMethodInfo {
	private MetricsComponentInfo componentInfo;
	private String name;
	private int operationId;
	boolean logInvocation;
	private boolean logErrors;
	private boolean logExceptions;
	private boolean countInvocation;
	private boolean countErrors;
	private boolean countExceptions;
	private String invocationCounterName;
	private String errorCounterName; // Controlled error returned in ApiResult.status
	private String exceptionCounterName;
	
	public MetricsMethodInfo(@NotNull MetricsComponentInfo componentInfo) {
		this.componentInfo = componentInfo;
	}

	// ... Other info needed for tracking and counting

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getOperationId() {
		return operationId;
	}

	public void setOperationId(int operationId) {
		this.operationId = operationId;
	}

	public boolean isLogInvocation() {
		return logInvocation;
	}

	public void setLogInvocation(boolean logInvocation) {
		this.logInvocation = logInvocation;
	}

	public boolean isLogErrors() {
		return logErrors;
	}

	public void setLogErrors(boolean logErrors) {
		this.logErrors = logErrors;
	}

	public boolean isLogExceptions() {
		return logExceptions;
	}

	public void setLogExceptions(boolean logExceptions) {
		this.logExceptions = logExceptions;
	}

	public boolean isCountInvocation() {
		return countInvocation;
	}

	public void setCountInvocation(boolean countInvocation) {
		this.countInvocation = countInvocation;
	}

	public boolean isCountErrors() {
		return countErrors;
	}

	public void setCountErrors(boolean countErrors) {
		this.countErrors = countErrors;
	}

	public boolean isCountExceptions() {
		return countExceptions;
	}

	public void setCountExceptions(boolean countExceptions) {
		this.countExceptions = countExceptions;
	}

	public String getInvocationCounterName() {
		return invocationCounterName;
	}

	public void setInvocationCounterName(String invocationCounterName) {
		this.invocationCounterName = invocationCounterName;
	}

	public String getErrorCounterName() {
		return errorCounterName;
	}

	public void setErrorCounterName(String errorCounterName) {
		this.errorCounterName = errorCounterName;
	}

	public String getExceptionCounterName() {
		return exceptionCounterName;
	}

	public void setExceptionCounterName(String exceptionCounterName) {
		this.exceptionCounterName = exceptionCounterName;
	}
	
	public void assignDefaultCounterNames() {
		if (countInvocation) {
			invocationCounterName = String.format("%s.%s.invocations", componentInfo.getName(), name);
		}
		// TODO: other counter names...
	}

}