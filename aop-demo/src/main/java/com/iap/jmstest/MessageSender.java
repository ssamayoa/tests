package com.iap.jmstest;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class MessageSender {

	private static Logger logger = LoggerFactory.getLogger(MessageSender.class);
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Scheduled(fixedRate = 5000, initialDelay = 10000)
	public void send() { 
		String message = "Hello ! " + (new Date());
		logger.info(message);
		jmsTemplate.convertAndSend("jms/testQueue", message);
	}
}
