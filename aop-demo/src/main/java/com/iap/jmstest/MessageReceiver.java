package com.iap.jmstest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class MessageReceiver {
	private static Logger logger = LoggerFactory.getLogger(MessageReceiver.class);

	@JmsListener(destination = "jms/testQueue") // , containerFactory = "myFactory")
	public void receiveMessage(String message) {
		logger.info("Received <{}>", message);
	}
}
