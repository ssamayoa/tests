package com.iap.othercontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/bye")
public class ByeController {

	private static Logger logger = LoggerFactory.getLogger(ByeController.class);

	@RequestMapping(value = "/{name}", method = RequestMethod.GET)
	public String bye(@PathVariable String name) {
		logger.debug("bye " + name); // NOSONAR 
		return "Bye " + name + "!";
	}
}
