package com.ericsson.controller;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.ericsson.entity.Customer;
import com.ericsson.service.CustomerService;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerControllerTestsV0 {

	@Autowired
	private TestRestTemplate restTemplate;

	@MockBean
	private CustomerService customerService;

	@Autowired
	private CustomerController customerController;

	@Before
	public void beforeTest() {
		ReflectionTestUtils.setField(customerController, "customerService", customerService);
	}

	@Test
	public void test_010_list() throws Exception {
		// Set up mock:
		List<Customer> expected = new ArrayList<>();
		Customer customer = new Customer();
		customer.setId(1l);
		customer.setName("George Bush");
		customer.setBalance(new BigDecimal("1000"));
		expected.add(customer);
		when(customerService.findAll()).thenReturn(expected);
		// Invoke then check that is a JSON array and first element
		// has id, name and balance properties.
		String json = restTemplate.getForObject("/api/customer", String.class);
		Object document = Configuration.defaultConfiguration().jsonProvider().parse(json);
		assertThat(JsonPath.read(document, "$.length()"), is(1));
		Object cust = JsonPath.read(document, "$.[0]");
		assertThat(JsonPath.read(cust, "$.id"), instanceOf(Number.class));
		assertThat(JsonPath.read(cust, "$.name"), instanceOf(String.class));
		assertThat(JsonPath.read(cust, "$.balance"), instanceOf(Number.class));
	}
}
