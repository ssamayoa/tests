package com.ericsson.controller;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.ericsson.entity.Customer;
import com.ericsson.service.CustomerService;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerControllerTestsV3 {

	private MockMvc mockMvc;

	@Mock
	private CustomerService customerService;

	@InjectMocks
	private CustomerController customerController;

	@Before
	public void beforeTest() {
		mockMvc = MockMvcBuilders.standaloneSetup(customerController).build();
	}

	@Test
	public void test_010_list() throws Exception {
		// Set up mock:
		List<Customer> expected = new ArrayList<>();
		Customer customer = new Customer();
		customer.setId(1l);
		customer.setName("George Bush");
		customer.setBalance(new BigDecimal("1000"));
		expected.add(customer);
		when(customerService.findAll()).thenReturn(expected);
		// Invoke then check that is a JSON array and first element
		// has id, name and balance properties.
		// @formatter:off
		mockMvc.perform(get("/api/customer")).andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.length()", is(1)))
				.andExpect(jsonPath("$.[0].id", allOf(notNullValue(), instanceOf(Number.class))))
				.andExpect(jsonPath("$.[0].name", allOf(notNullValue(), instanceOf(String.class))))
				.andExpect(jsonPath("$.[0].balance", allOf(notNullValue(), instanceOf(Number.class))))
				.andReturn();
		// @formatter:on
	}

}
