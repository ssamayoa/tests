package com.ericsson.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import com.ericsson.dto.HighBalanceDto;
import com.ericsson.entity.Customer;
import com.ericsson.entity.HighBalance;
import com.ericsson.repository.CustomerRepository;
import com.ericsson.repository.HighBalanceRepository;
import com.ericsson.service.impl.HighBalanceListener;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HighBalanceListenerTestsV2 {

	@Mock
	private HighBalanceRepository highBalanceRepository;

	@Mock
	private CustomerRepository customerRepository;

	@Mock
	private RestTemplate restTemplate;

	@InjectMocks
	private HighBalanceListener highBalanceListener;

	@Before
	public void beforeTest() {
		ReflectionTestUtils.setField(highBalanceListener, "highBalanceUrl", "http://localhost:9001/highbalance");
		ReflectionTestUtils.setField(highBalanceListener, "restTemplate", restTemplate);
	}

	@Test
	public void test_010_balance_above_1000() {
		// Set up mocks:
		Customer customer = new Customer();
		customer.setId(1l);
		when(customerRepository.findOne(1l)).thenReturn(customer);
		when(highBalanceRepository.findByCustomer(anyObject())).thenReturn(Optional.empty());
		// Set params
		HighBalanceDto highBalanceDto = new HighBalanceDto(1l, new BigDecimal("1001"));
		// Call
		highBalanceListener.receiveMessage(highBalanceDto);
		// Verify
		verify(highBalanceRepository).save((HighBalance) anyObject());
		verify(restTemplate).postForObject(anyString(), anyObject(), eq(String.class));
	}

	@Test
	public void test_020_balance_above_1000_exists() {
		// Set up mocks:
		Customer customer = new Customer();
		customer.setId(1l);
		when(customerRepository.findOne(1l)).thenReturn(customer);
		HighBalance highBalance = new HighBalance();
		highBalance.incCount();
		when(highBalanceRepository.findByCustomer(anyObject())).thenReturn(Optional.of(highBalance));
		// Set params
		HighBalanceDto highBalanceDto = new HighBalanceDto(1l, new BigDecimal("1001"));
		// Call
		highBalanceListener.receiveMessage(highBalanceDto);
		// Verify
		verify(highBalanceRepository).save((HighBalance) anyObject());
		verify(restTemplate, never()).postForObject(anyString(), anyObject(), eq(String.class));
		// Assert
		assertThat(highBalance.getCount(), is(2));
	}

	@Test
	public void test_030_balance_below_1000() {
		// Set up mocks:
		Customer customer = new Customer();
		customer.setId(1l);
		when(customerRepository.findOne(1l)).thenReturn(customer);
		HighBalance highBalance = new HighBalance();
		highBalance.incCount();
		when(highBalanceRepository.findByCustomer(anyObject())).thenReturn(Optional.of(highBalance));
		// Set params
		HighBalanceDto highBalanceDto = new HighBalanceDto(1l, new BigDecimal("100"));
		// Call
		highBalanceListener.receiveMessage(highBalanceDto);
		// Verify
		verify(highBalanceRepository, never()).save((HighBalance) anyObject());
		verify(restTemplate, never()).postForObject(anyString(), anyObject(), eq(String.class));
	}

	@Test
	public void test_040_balance_below_1000_exists_dec() {
		// Set up mocks:
		Customer customer = new Customer();
		customer.setId(1l);
		when(customerRepository.findOne(1l)).thenReturn(customer);
		HighBalance highBalance = new HighBalance();
		highBalance.incCount();
		when(highBalanceRepository.findByCustomer(anyObject())).thenReturn(Optional.of(highBalance));
		// Set params
		HighBalanceDto highBalanceDto = new HighBalanceDto(1l, new BigDecimal("100"));
		// Call
		highBalanceListener.receiveMessage(highBalanceDto);
		// Verify
		verify(highBalanceRepository).delete(any(HighBalance.class));
		verify(restTemplate, never()).postForObject(anyString(), anyObject(), eq(String.class));
		// Assert
		assertThat(highBalance.getCount(), is(0));
	}
}