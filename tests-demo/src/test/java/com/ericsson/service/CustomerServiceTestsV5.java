package com.ericsson.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.ericsson.entity.Customer;
import com.ericsson.repository.CustomerRepository;
import com.ericsson.service.impl.CustomerServiceImpl;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerServiceTestsV5 {

	@Mock
	private CustomerRepository customerRepository;

	@Mock
	private JmsTemplate jmsTemplate;

	private CustomerService customerService;

	@Before
	public void beforeTest() {
		customerService = new CustomerServiceImpl();
		ReflectionTestUtils.setField(customerService, "customerRepository", customerRepository);
		ReflectionTestUtils.setField(customerService, "jmsTemplate", jmsTemplate);
	}

	@Test
	public void test_010_list() throws Exception {
		// Set up mock:
		List<Customer> expected = new ArrayList<>();
		Customer customer = new Customer();
		customer.setId(1l);
		customer.setName("George Bush");
		customer.setBalance(new BigDecimal("1000"));
		expected.add(customer);
		when(customerRepository.findAll()).thenReturn(expected);
		// Call
		List<Customer> response = customerService.findAll();
		assertThat(response.size(), is(expected.size()));
	}

	@Test
	public void test_020_create() {
		// Set up mock:
		Customer expected = new Customer();
		expected.setId(1l);
		expected.setName("George Bush");
		expected.setBalance(new BigDecimal("1001"));
		when(customerRepository.save((Customer) any())).thenReturn(expected);
		// Set param data:
		Customer newCustomer = new Customer();
		newCustomer.setName("George Bush");
		newCustomer.setBalance(new BigDecimal("1001"));
		// Call
		Customer response = customerService.create(newCustomer);
		// Assert
		assertThat(response, is(expected));
	}

	@Test(expected = EmptyResultDataAccessException.class)
	public void test_030_update_id_null() {
		// Set param data:
		Customer customer = new Customer();
		// Call
		customerService.update(customer);
	}

	@Test(expected = EmptyResultDataAccessException.class)
	public void test_040_update_not_exists() {
		// Set up mock:
		when(customerRepository.exists(any(Long.class))).thenReturn(false);
		// Set param data:
		Customer customer = new Customer();
		customer.setId(1l);
		// Call
		customerService.update(customer);
	}
	
	
}
