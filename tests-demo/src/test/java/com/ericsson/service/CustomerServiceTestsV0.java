package com.ericsson.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import com.ericsson.entity.Customer;
import com.ericsson.repository.CustomerRepository;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerServiceTestsV0 {

	@Mock
	private CustomerRepository customerRepository;

	@InjectMocks
	private CustomerService customerService;

	@Test
	public void test_010_list() throws Exception {
		// Set up mock:
		List<Customer> expected = new ArrayList<>();
		Customer customer = new Customer();
		customer.setId(1l);
		customer.setName("George Bush");
		customer.setBalance(new BigDecimal("1000"));
		expected.add(customer);
		when(customerRepository.findAll()).thenReturn(expected);
		// Call
		List<Customer> response = customerService.findAll();
		// Assert
		assertThat(response.size(), is(expected.size()));
	}
}
