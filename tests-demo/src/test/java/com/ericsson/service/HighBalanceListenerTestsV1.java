package com.ericsson.service;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.ericsson.dto.HighBalanceDto;
import com.ericsson.entity.Customer;
import com.ericsson.entity.HighBalance;
import com.ericsson.repository.CustomerRepository;
import com.ericsson.repository.HighBalanceRepository;
import com.ericsson.service.impl.HighBalanceListener;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HighBalanceListenerTestsV1 {

	@Mock
	private HighBalanceRepository highBalanceRepository;

	@Mock
	private CustomerRepository customerRepository;

	@InjectMocks
	private HighBalanceListener highBalanceListener;

	@Before
	public void beforeTest() {
		ReflectionTestUtils.setField(highBalanceListener, "highBalanceUrl", "http://localhost:9001/highbalance");
	}

	@Test
	public void test_010_balance_above_1000() {
		// Set up mocks:
		Customer customer = new Customer();
		customer.setId(1l);
		when(customerRepository.findOne(1l)).thenReturn(customer);
		when(highBalanceRepository.findByCustomer(anyObject())).thenReturn(Optional.empty());
		// Set params
		HighBalanceDto highBalanceDto = new HighBalanceDto(1l, new BigDecimal("1001"));
		// Call
		highBalanceListener.receiveMessage(highBalanceDto);
		// Verify
		verify(highBalanceRepository).save((HighBalance) anyObject());
	}
}
