package com.ericsson.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ericsson.entity.Customer;
import com.ericsson.service.CustomerService;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@RequestMapping(method = RequestMethod.GET)
	public List<Customer> list() {
		return customerService.findAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	public Customer create(@RequestBody Customer customer) {
		return customerService.create(customer);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public Customer update(@RequestBody Customer customer) {
		return customerService.update(customer);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id) {
		customerService.delete(id);
	}

}
