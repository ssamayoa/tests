package com.ericsson.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class HighBalanceDto implements Serializable {
	private static final long serialVersionUID = 3585611391691676656L;

	private Long customerId;
	private BigDecimal balance;

	public HighBalanceDto() {
	}

	public HighBalanceDto(Long customerId, BigDecimal balance) {
		this.customerId = customerId;
		this.balance = balance;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

}
