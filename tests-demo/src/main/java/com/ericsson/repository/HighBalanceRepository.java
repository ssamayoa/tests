package com.ericsson.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ericsson.entity.Customer;
import com.ericsson.entity.HighBalance;

public interface HighBalanceRepository extends JpaRepository<HighBalance, Long> {

	Optional<HighBalance> findByCustomer(Customer customer);
}
