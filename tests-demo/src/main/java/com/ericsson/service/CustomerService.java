package com.ericsson.service;

import java.util.List;

import com.ericsson.entity.Customer;

public interface CustomerService {
	String JMS_HIGH_BALANCE = "jms/highBalance";

	List<Customer> findAll();

	Customer create(Customer customer);

	Customer update(Customer customer);

	void delete(Long id);

}
