package com.ericsson.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.ericsson.dto.HighBalanceDto;
import com.ericsson.entity.Customer;
import com.ericsson.repository.CustomerRepository;
import com.ericsson.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {
	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private JmsTemplate jmsTemplate;

	@Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}

	@Override
	public Customer create(Customer customer) {
		customer = customerRepository.save(customer);
		jmsTemplate.convertAndSend(JMS_HIGH_BALANCE, new HighBalanceDto(customer.getId(), customer.getBalance()));
		return customer;
	}

	/**
	 * <p>
	 * Updates the received customer.
	 * </p>
	 * <p>
	 * Customer must exists else EmptyResultDataAccessException is thrown
	 * </p>
	 */
	@Override
	public Customer update(Customer customer) {
		if (customer.getId() == null || !customerRepository.exists(customer.getId())) {
			throw new EmptyResultDataAccessException(String.format("Customer id=%d does not exists.", customer.getId()),
					0);
		}
		customer = customerRepository.save(customer);
		jmsTemplate.convertAndSend(JMS_HIGH_BALANCE, new HighBalanceDto(customer.getId(), customer.getBalance()));
		return customer;
	}

	/**
	 * <p>
	 * Deletes the customer with the received id.
	 * </p>
	 * <p>
	 * Customer must exists else EmptyResultDataAccessException is thrown
	 * </p>
	 */
	@Override
	public void delete(Long id) {
		if (!customerRepository.exists(id)) {
			throw new EmptyResultDataAccessException(String.format("Customer id=%d does not exists.", id), 0);
		}
		customerRepository.delete(id);
	}

}
