package com.ericsson.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ericsson.dto.HighBalanceDto;
import com.ericsson.entity.Customer;
import com.ericsson.entity.HighBalance;
import com.ericsson.repository.CustomerRepository;
import com.ericsson.repository.HighBalanceRepository;
import com.ericsson.service.CustomerService;

@Service
public class HighBalanceListener {
	private static Logger logger = LoggerFactory.getLogger(HighBalanceListener.class);

	@Autowired
	private HighBalanceRepository highBalanceRepository;

	@Autowired
	private CustomerRepository customerRepository;

	private RestTemplate restTemplate;

	@Value("${high.balance.url}")
	private String highBalanceUrl;

	public HighBalanceListener() {
		restTemplate = new RestTemplate();
	}

	@JmsListener(destination = CustomerService.JMS_HIGH_BALANCE)
	public void receiveMessage(HighBalanceDto highBalanceDto) {
		logger.debug("Got message: customerId:{}, balance:{}", highBalanceDto.getCustomerId(),
				highBalanceDto.getBalance());
		Customer customer = customerRepository.findOne(highBalanceDto.getCustomerId());
		Optional<HighBalance> optionalHighBalance = highBalanceRepository.findByCustomer(customer);
		if (highBalanceDto.getBalance() != null && highBalanceDto.getBalance().intValue() > 1000) {
			HighBalance highBalance;
			if (optionalHighBalance.isPresent()) {
				highBalance = optionalHighBalance.get();
			} else {
				highBalance = new HighBalance(customer);
			}
			highBalance.incCount();
			highBalanceRepository.save(highBalance);
			if (highBalance.getCount() == 1) {
				if (highBalanceUrl == null) {
					logger.warn("highBalanceUrl not definded.");
				} else {
					try {
						restTemplate.postForObject(highBalanceUrl, highBalanceDto, String.class);
					} catch (Exception e) {
						logger.error("{}", highBalanceUrl, e);
					}
				}
			}
		}
		if (highBalanceDto.getBalance() != null && highBalanceDto.getBalance().intValue() < 1000
				&& optionalHighBalance.isPresent()) {
			HighBalance highBalance = optionalHighBalance.get();
			highBalance.decCount();
			if (highBalance.getCount() == 0) {
				highBalanceRepository.delete(highBalance);
			}
		}
	}
}
